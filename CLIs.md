---
title: "LARBS"
layout: single
---

{{< img src="/pix/larbs.png" class=normal >}}

LARBS is an efficient shell script that will install a fully-featured tiling window manager-based system on any Arch or [Artix](https://artixlinux.org) Linux-based system, without any of the routine of manual post-install processes and configuration.

## Two types of LARBS users

Is LARBS for you? Probably yes. I don't know how else you would've found this site. The script is for two types of people:

1. People who already know their stuff and just want to automate installing a system without doing the boring stuff you've done a million times.
2. Novices who want to use and learn about a leet hackerman computer setup like those in the movies for either efficiency or looking cool.


No actual phonies allowed though.
The goal of the system for novices is helping you understand how a good Unix system works and how it is modified.
I give huge amounts of documentation for this, but this is not a hand-holding distro that does things automatically for you.
Instead, you realize how easy it is to set things up automatically yourself.

## Installation

On a fresh installation of Arch Linux or Artix Linux, run the following:

```fish
curl -LO larbs.xyz/larbs.sh
sh larbs.sh
```

LARBS will then guide you through installation, which is typically relatively snappy. On my slow internet, it takes around 10 minutes.

Note that the LARBS scripts will not partition any drives or wipe anything, **but** when it deploys the dotfiles, it will overwrite any preexisting files: e.g. the LARBS bashrc will replace your old bashrc, etc. To avoid even this risk, you can tell LARBS to install for a new username and nothing will be overwritten.

## No un-features

- No proprietary software or spyware.
- No snaps or flatpaks or Mac-lite garbage. GNU/Linux the way it's supposed to be.
- No branding cringe. Once you run LARBS, you have **your own** system, not mine!

## Programs

Here are the main programs, all with extra information here:

- [dwm](/dwm) -- the main graphical environment
- [st](/st) -- the terminal
- [dwmblocks](/dwmblocks) -- statusbar
- [zsh](zsh) -- the shell
- [Librewolf](librewolf) with the Arkenfox.js -- browser
- [lf](/lf) -- file manager
- [neomutt](/neomutt) -- email
- [abook](/abook) -- extensible address book
- [ncmpcpp](/ncmpcpp) -- music
- [newsboat](newsboat) -- RSS feeds and news
- [htop](htop) -- to look cool in screencaps... err... system monitor
- [mpv](/mpv) -- video player
- nsxiv -- image viewer

## Learning the system is fun and easy!

You can figure out about the system in a lot of different ways:

- LARBS has a built-in readme document and list of all the many efficient keybindings that you can read [here](/larbs-dwm.pdf). By pressing <kbd>super + F1</kbd> at any time while in the system you can read this document.
- The many illustrative videos on [Luke's YouTube channel](https://youtube.com/lukesmithxyz), some of which are easily watchable in LARBS by pressing <kbd>super + F2</kbd>.
- The documentation on the <a href="https://github.com/lukesmithxyz/voidrice">Github</a> page.
- By just installing it and diving in!





---
title: "Newsboat"
date: 2023-01-19T11:37:10-05:00
---

Newsboat is an RSS-reader.

A normal person asks: "What is an RSS reader?"

It's a way to follow sites and social media updates without needing social media.

## Documentation

`man newsboat`

## Running

Press <kbd>super + shift + n</kbd> or run `newsboat` in the terminal.

## Bindings

- <kbd>j</kbd>/<kbd>k</kbd> -- move up and down.
- <kbd>enter</kbd> -- enter feed or article.
- <kbd>q</kbd> -- return to previous screen or quit.
- <kbd>h</kbd>/<kbd>l</kbd> -- open feed or return (same as <kbd>q</kbd> and <kbd>enter</kbd>).
- <kbd>a</kbd> -- mark as read.
- <kbd>n</kbd> -- go to next unread.
- <kbd>A</kbd> -- mark all as read.
- <kbd>,,</kbd> -- open the main link of an article. (Usually opens in a browser, or if a video, will play the video in [mpv](mpv).)
- <kbd>u</kbd>/<kbd>d</kbd> -- page up and down.
- <kbd>g</kbd>/<kbd>G</kbd> -- go to top or bottom of screen.

To follow a visible link, use the [st](st) binding <kbd>alt + l</kbd>, or to just copy it, <kbd>alt + k</kbd>.


## Files

- `~/.config/newsboat/urls` -- the file holding your RSS feeds. Add URLs here to make them appear in newsboat. Open itquickly by typing `cfu` in the terminal.
- `~/.config/newsboat/config` -- the general newsboat config. Type `cfn` in the terminal. You can set granulated coloring and effects here, as well as change key bindings.
- `~/.local/bin/linkhandler` -- the opener file used by newsboat for dealing with URLs with the <kbd>,,</kbd> binding. This can be modified as needed. By default, it opens normal URLs in a browser, opens videos with [mpv](mpv), downloads audio/podcast files and downloads and opens images with [sxiv](sxiv), etc.

## Links

- [newsboat website](https://newsboat.org/)
- MIT License




---
title: "Neomutt"
date: 2023-01-16T16:15:58-05:00
---

Neomutt is a fast terminal-based email client.

{{< img src="/pix/neomutt.png" class=normal >}}

## Setup with Mutt Wizard

Use [mutt-wizard](https://muttwizard.com) (`mw`) to add mail accounts. This is already installed on LARBS. Added accounts will be accessible from neomutt. To add your first account, just run the following:

```fish
mw -a your@email.com
```

Once you have installed accounts, sync them with the command `mailsync`, or by pressing <kbd>super + F8</kbd>.

## Running

Press <kbd>super + e</kbd> or type `neomutt` manually in the terminal.


## Bindings

Note also that neomutt automatically documents itself: you can press <kbd>?</kbd> at any time in the program and you will see a list of all shortcuts for the screen you are on.

### In the mail index...

- <kbd>j</kbd>, <kbd>k</kbd> -- move up and down in mail.
- <kbd>l</kbd> or <kbd>enter</kbd> -- open mail, or return to index.
- <kbd>h</kbd> or <kbd>esc</kbd> -- return from mail to index.
- <kbd>m</kbd> -- compose new mail.
- <kbd>r</kbd> -- reply to selected mail.
- <kbd>R</kbd> -- reply all to selected mail.
- <kbd>ctrl-j</kbd>/<kbd>ctrl-k</kbd> -- move up or down in the sidebar.
- <kbd>ctrl-o</kbd> -- open the box highlighted in the sidebar.
- <kbd>space</kbd> -- select mail. (See below).

mutt-wizard automatically sets automatic binds to move to or move mail to other boxes. Press one of these keys:

- <kbd>g</kbd> -- go to...
- <kbd>M</kbd> -- move selected mail to...
- <kbd>C</kbd> -- copy selected mail to...

And then follow it with one of these keys:

- <kbd>i</kbd> -- ...the inbox.
- <kbd>d</kbd> -- ...drafts.
- <kbd>s</kbd> -- ...the sent box.
- <kbd>a</kbd> -- ...the archive.
- <kbd>j</kbd> -- ...junk.

### With mail open...

Some binds above are shared.

- <kbd>j</kbd>/<kbd>k</kbd> -- scroll up and down.
- <kbd>J</kbd>/<kbd>K</kbd> -- next or previous mail.
- <kbd>v</kbd> -- view attachments. This can be used for opening annoying HTML ad emails in the browser. You can also save the selected attachment with <kbd>s</kbd>.

### On the compose screen...

- <kbd>a</kbd> -- attach a file.
- <kbd>m</kbd> -- edit highlighted attachment in text editor.
- <kbd>S</kbd> -- choose to sign or encrypt mail (usually with PGP).
- <kbd>y</kbd> -- send mail.

## Configuration

- `~/.config/mutt/muttrc` -- main configuration file
- `~/.config/mutt/accounts/` -- where mutt-wizard will put account-specific configuration files, named after your email addresses
- `~/.local/share/mail/` -- where your mail is stored.

## Extending

Neomutt is one of the most extensible programs on the planet. I am constantly surprised by what you can do with it. If you are ever bored, yet hungry for efficiency, check out the neomutt and neomuttrc manuals.

One nice little thing is how widely aliases can be used. For example, we can add a line like this to our muttrc:

```muttrc
alias besties luke@email.com, richard@email.com, linus@email.com
```

This aliases the sequence `besties` to the following text/email addresses. So we can just type `besties` as a recipient of a mail, and the other addresses will be filled in.

Note also that neomutt is configured to tab complete contact information stored with [abook](/abook) automatically.

## Documentation

`man neomutt` or `man neomuttrc`.

[Mutt/Neomutt on the ArchWiki](https://wiki.archlinux.org/title/Mutt)


---
title: "mpv"
date: 2023-01-17T11:39:29-05:00
---

mpv is a video-player so simple, it really just shows the video.
It is controlled by the keyboard and only shows even the progress bar and other interfaces when needed.

It is also used in LARBS to play single audio files if [mpd](/mpd) is not used.


## Documentation

`man mpv`

## Running

mpv is opened automatically when you select a video or audio file to open from [lf](/lf) or another program.
Obviously you can run it from the command line by running `mpv filename.mp4`, etc.

## Bindings

These are custom bindings for ease:

- <kbd>h</kbd>/<kbd>l</kbd> -- jump back/foward 10 seconds.
- <kbd>j</kbd>/<kbd>k</kbd> -- jump back/foward 60 seconds.
- <kbd>space</kbd> -- toggle pause.
- <kbd>S</kbd> -- toggle subtitles.
- <kbd>o</kbd> --  briefly view progress bar and time.
- <kbd>O</kbd> -- toggle time visibility.
- <kbd>i</kbd> -- show file and video information.

## Configuration

- `~/.config/mpv/input.conf` -- key bindings.
- `~/.config/mpv/` -- many other plugins can be added to mpv.

## Other

- If you download films, I recommend installing the program `subdl`, which you can run on a movie file and it will automatically check online for subtitle files. Give it the `-i` option to choose from the closest matches if the first result is slightly off.
- The LARBS binding <kbd>super + shift + P</kbd> will not only pause you music played in `mpd`, but also all audio/video instances of `mpv`.
- Press <kbd>super + F11</kbd> to see your webcam. This uses an instance of `mpv` as well.

GPLv2


---
title: "htop"
date: 2023-01-19T10:38:41-05:00
---

htop is an absolutely useless program.

{{< img src="/pix/htop.png" link="/pix/htop.png" class=normal >}}

## What is htop?

htop is theoretically a system-monitor program, but no one uses it for that.

What it actually is is a program that produces a bunch of smart-looking and multicolor lines and shapes with important-seeming process names.

This is the ultimate weapon against impressionable normies if you want to impress them by pretending to be some kind of hacker.

htop is only found (1) in screencaps to show your "set-up" to other losers on the internet and (2) when a normie girl is nearby to hopefully goad her into a conversation you were otherwise too awkward to initiate yourself.

## You wouldn't do it in Windows.

Look at how stupid this image looks.

{{< img src="/pix/windows-htop.jpg" link="/pix/windows-htop.jpg" class=normal >}}

"Oh what, he just pulled up some command prompts, a file browser and a system monitor? Why? Is he actually doing something with those empty prompts? Why does he need a system monitor filling up a third of his screen for this alleged work he's doing?"

All sensible questions that a person naturally asks when he sees the familiar world of Windows.

Yet for newfriends who HECKIN' LOVE GNU/LINUX, for what ever reason, they do the same stuff, take a screencap of it and post it.

This is only possible because even of those people use use GNU/Linux, even in a fancy tiling window manager, so many of them are still struggling to figure things out, so a couple htop windows and a silly file manager open and the brain degrades to a lower, confusion-induced operating level which makes it fawn at the mystery of it.

I mean really---obviously I think tiling window managers are useful, that's what LARBS is about, but when you are *actually working on something* do you *ever* have more than three windows tiled on the same workspace at one time?

Seriously, for most things, I have one thing per workspace unless another prompt is strictly visually necessary. Three or *maybe* four is definitely the maximum.

So it goes without saying that people who pull up seven windows and some pics of anime girls baking pancakes are not doing anything with their computers looking like that.

## Running

Run `htop` by typing it in the terminal.
I even bound it to <kbd>super + shift + r</kbd> just for fun because I wasn't using that key for anything important.

---
title: "Abook"
date: 2023-01-17T11:54:20-05:00
---

The address book.

Why let Google or Apple keep your contacts? Keep an archive on your computer and export them when you need them.
Abook is a minimal and local store of your contacts, including emails, phone numbers, addressess and whatever you might need.
Abook integrates with neomutt's autofill as well and you can script more functionality into it.

## Documentation

`man abook`

## Running

Press <kbd>super + shift + e</kbd> or run `abook` in the terminal.

## Bindings

Thankfully, abook is self-documenting: press <kbd>?</kbd> at any time to see its binds. Here are some of the most common:

- <kbd>a</kbd> -- add a contact.
- <kbd>j</kbd>/<kbd>k</kbd> -- move up and down.
- <kbd>enter</kbd> -- enter a contact.
- <kbd>h</kbd>/<kbd>l</kbd> -- (on a contact page) move from tab to tab.
- <kbd>0-9</kbd> -- (on a contact page) edit/change contact information in a field.
- <kbd>d</kbd> -- delete a contact.
- <kbd>D</kbd> -- duplicate a contact.
- <kbd>S</kbd> -- sort existing contacts.

## Extending

Using with email
: abook works out of the box with [neomutt](/neomutt). In neomutt, when you compose a new message, if you press tab, neomutt will automatically tab-complete from your abook contacts.

Using telephone numbers
: There are some services that offer computer-based calling. Since abook can export a list of contacts with data, you can use programs like dmenu to select one you want:
```fish
abook --convert --infile ~/.abook/addressbook --outformatstr="\!{name} {mobile}" --outformat=custom  | dmenu -i -l 30
```
This can easily be integrated into a script.


## Links

- [website](https://abook.sourceforge.io/)
- GPLv2

